import math
import random

from PyQt5.QtCore import QSize, QPointF, QRectF, QTimer, QElapsedTimer, Qt
from PyQt5.QtGui import QPalette, QPainter, QBrush
from PyQt5.QtWidgets import QApplication, QWidget, QHBoxLayout, QPushButton


def magQPointF(p):
    return math.sqrt(p.x()*p.x() + p.y() * p.y())


def scale3_2(x1, x2, x3, y1, y2):
    return y1 + (y2-y1)*(x2-x1)/(x3-x1)


def normalizeQPointF(p):
    p2 = QPointF(p)
    return p2/magQPointF(p)


class Planetoid2:
    def __init__(self, pos, speed, mass, color):
        self.pos = pos
        self.speed = speed
        self.cutoff = 100
        self.mass = mass
        self.pastPos = []
        self.color = color
        self.trailSkip = 5
        self.currentNumber = 0

    def updatePosition(self):
        if self.currentNumber >= self.trailSkip:
            if len(self.pastPos) > 20:
                self.pastPos.pop(0)
            self.pastPos.append(QPointF(self.pos.x(), self.pos.y()))
            self.currentNumber = 0
        else:
            self.currentNumber += 1
        self.pos += self.speed

    def updateSpeed(self, p2):
        force = self.computeForce(p2)
        accel = force/self.mass
        self.speed += accel

    def computeForce(self, p2):
        """Compute the change in velocity of self due to pull of p2
            steps:  Choose method of finding velocity by finding distance
                    between self and p2, computeForce1 or computeForce2.
        """
        r = p2.pos - self.pos
        l = magQPointF(r)
        if l < self.cutoff:
            return self.computeForce2(p2)
        else:
            return self.computeForce1(p2)

    def computeForce2(self, p2):
        """Compute change in velocity in the case of a close planetoid.
            Computes force as a linear function of distance.
        """
        r = p2.pos - self.pos
        u = normalizeQPointF(r)
        l = magQPointF(r)
        y = self.mass * p2.mass/self.cutoff**2
        m = y/self.cutoff
        f = m * l * u
        return f

    def computeForce1(self, p2):
        """Compute change in velocity in the case of a far Planetoid
            Computes force using the function 1/distance^2 * gravitational
            constant
        """
        r = p2.pos - self.pos
        u = normalizeQPointF(r)
        l = magQPointF(r)
        f = u * self.mass * p2.mass/l**2
        return f

    def draw(self, painter):
        painter.setBrush(QBrush(self.color))
        for p in self.pastPos:
            r = QRectF(p - QPointF(2, 2), p + QPointF(2, 2))
            painter.drawEllipse(r)
        p = self.pos
        r = QRectF(p - QPointF(10, 10),
                   p + QPointF(10, 10))
        painter.drawEllipse(r)


class RenderArea(QWidget):
    def __init__(self, parent=None):
        super(RenderArea, self).__init__(parent)
        self.setBackgroundRole(QPalette.Base)
        self.setAutoFillBackground(True)
        self.t = 0
        pt1 = Planetoid2(QPointF(501, 501),
                         QPointF(.3, .3),
                         25,
                         Qt.red)
        pt2 = Planetoid2(QPointF(500, 301),
                         QPointF(-.5, 0),
                         50,
                         Qt.blue)
        pt3 = Planetoid2(QPointF(500, 500),
                         QPointF(-.3, .3),
                         25,
                         Qt.green)
        self.pList = [pt1, pt2, pt3]

        def p():
            return self.makeRandomPlanetoid(100, 800, 100, 600, -.5, .5,
                                            [5, 10, 15],
                                            [Qt.red, Qt.blue, Qt.green])
        for i in range(6):
            self.pList.append(p())

    def makeRandomPlanetoid(self, minX, minY, maxX, maxY,
                            minV, maxV, massList, colorList):
        return Planetoid2(QPointF(random.uniform(minX, maxX), random.uniform(minY, maxY)),
                          QPointF(random.uniform(minV, maxV), random.uniform(minV, maxV)),
                          random.choice(massList),
                          random.choice(colorList))

    def sizeHint(self):
        return QSize(800, 800)

    def timeUpdate(self, t):
        self.t = t
        for i in range(len(self.pList)):
            for j in range(len(self.pList)):
                if i != j:
                    self.pList[i].updateSpeed(self.pList[j])

        for p in self.pList:
            p.updatePosition()

        self.update()

    def paintEvent(self, event):
        pnt = QPainter(self)
        pnt.setRenderHint(QPainter.Antialiasing)
        pnt.setPen(Qt.NoPen)

        for p in self.pList:
            p.draw(pnt)


class Window(QWidget):
    def __init__(self):
        super(Window, self).__init__()
        self.renderArea = RenderArea()
        lo = QHBoxLayout()
        # pb = QPushButton("words")
        lo.addWidget(self.renderArea)
        self.setLayout(lo)
        self.setWindowTitle('window')
        self.elapsedTimer = QElapsedTimer()
        self.elapsedTimer.start()

        self.timer = QTimer(self)
        self.timer.setSingleShot(False)
        self.timer.timeout.connect(self.timeUpdate)
        self.timer.start(1000/120)

    def timeUpdate(self):
        self.renderArea.timeUpdate(self.elapsedTimer.elapsed())


if __name__ == '__main__':
    app = QApplication([])
    win = Window()
    win.show()
    app.exec_()
