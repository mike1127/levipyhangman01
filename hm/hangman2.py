def checkIfComplete(w):
    return all([t[1] for t in w])


def checkIfIncomplete(w):
    return any([not t[1] for t in w])

def printRepr(wordTuples):
    rep = [t[0] if t[1] else ' _ ' for t in wordTuples]
    print("".join(rep))

def main():
    word = "banana"
    wordTuples = [(i.lower(), False) for i in list(word) ]
    while checkIfIncomplete(wordTuples):
        printRepr(wordTuples)
        inp = input("Type a character:")
        wordTuples = [(c, True) if c == inp else (c, f)
                      for (c,f) in wordTuples]

    printRepr(wordTuples)
    print("You won!")


main()

        
        
    
