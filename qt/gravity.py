from PyQt5.QtWidgets import QWidget, QHBoxLayout, QApplication
from PyQt5.QtCore import QRectF, QPointF, QSize, QTimer, Qt, QElapsedTimer
from PyQt5.QtGui import QPalette, QPen, QPainter, QBrush
import math


def magQPointF(p):
    return math.sqrt(p.x()*p.x() + p.y() * p.y())

def normalizeQPointF(p):
    p2 = QPointF(p)
    return p2/magQPointF(p)

'''
class Pos:

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __add__(self, other):
        return Pos(self.x + other.x, self.y+other.y)

    def __sub__(self, other):
        return Pos(self.x - other.x, self.y - other.y)

    def mult(self, r):
        return Pos(r*self.x, r*self.y)

    def mag(self):
        return math.sqrt(self.x*self.x + self.y*self.y)

    def normalized(self):
        return Pos(self.x/self.mag(), self.y/self.mag())
'''

def scale3_2(x1, x2, x3, y1, y2):
    return y1 + (y2-y1)*(x2-x1)/(x3-x1)

class Planetoid:

    def __init__(self, initPos, initVel, mass, movable=True):
        self.pos = initPos
        self.vel = initVel
        self.mass = mass
        self.movable = movable
        self.maxVel = 300.0

    def computePullToward(self, other):
        """Compute the pull this planetoid makes on the passed-in planetoid."""
        vec1 = self.pos - other.pos
        mag1 = magQPointF(vec1)
        gravityStrength = 200.0
        cutoff = 150.0
        powIndex = 1.8
        if mag1 < cutoff:
            m = gravityStrength*(self.mass + other.mass)/math.pow(cutoff, powIndex)
            forceMag = scale3_2(0.0, mag1, cutoff, -2*m, m)
        else:
            forceMag = 200*(self.mass + other.mass)/math.pow(mag1, powIndex)
        # scale magnitude down to zero
        return normalizeQPointF(vec1) * forceMag

    def draw(self, painter):
        painter.drawEllipse(QRectF(self.pos-QPointF(2, 2), self.pos + QPointF(10, 10)))

    def update(self, others, deltaT):
        if not self.movable:
            return
        f = QPointF(0, 0)
        for o in others:
            f = f + o.computePullToward(self)
        m = magQPointF(f)
        self.vel = self.vel + f
        if magQPointF(self.vel) > self.maxVel:
            self.vel = self.maxVel*normalizeQPointF(self.vel)
        self.pos = self.pos + deltaT*self.vel/4000





class RenderArea(QWidget):
    def __init__(self, parent=None):
        super(RenderArea, self).__init__(parent) 
        self.setBackgroundRole(QPalette.Base)

        self.setAutoFillBackground(True)
        self.antialiased = True
        p1 = Planetoid(QPointF(275, 275), QPointF( 30,   -120), 100, True)
        p2 = Planetoid(QPointF(400, 400), QPointF(0,   0), 200, False)
        p3 = Planetoid(QPointF(650, 550), QPointF(90,   -40), 100, True)
        p4 = Planetoid(QPointF(275, 600), QPointF(-90, 10), 100, True)
        self.ps = [p1, p2, p3, p4]
        self.t = 0


        # self.rotation = 0.0
        # self.t = 0.0
        # self.deltaT = -1.0
        # self.radius = 100.0
        # self.center = QPointF(200, 200)

    def minimumSizeHint(self):
        return QSize(400, 400)

    def sizeHint(self):
        return QSize(800, 800)

    def paintEvent(self, event):
        pnt = QPainter(self)
        pnt.setRenderHint(QPainter.Antialiasing)
        # pnt.drawLine(QPointF(0, 0), QPointF(100, 200))
        pnt.setBrush(QBrush(Qt.black))
        for p in self.ps:
            p.draw(pnt)
        # pnt.drawEllipse(QRectF(QPointF(0, 0), QPointF(100, 100)))

    def timeUpdate(self, t):
        deltaT = t-self.t
        self.t = t

        l = len(self.ps)
        for i in range(l):
            others = [self.ps[j] for j in range(l) if j != i]
            self.ps[i].update(others, deltaT)
        #
        self.update()


class Window(QWidget):
    def __init__(self):
        super(Window, self).__init__()
        self.renderArea = RenderArea()
        lo = QHBoxLayout()
        lo.addWidget(self.renderArea)
        self.setLayout(lo)
        self.setWindowTitle("Drawing")
        self.elapsedTimer = QElapsedTimer()
        self.elapsedTimer.start()
        
        self.timer = QTimer(self)
        self.timer.setSingleShot(False)
        self.timer.timeout.connect(self.timeUpdate)
        self.timer.start(60)

    def keyPressEvent(self, evt):
        pass

    def timeUpdate(self):
        self.renderArea.timeUpdate(self.elapsedTimer.elapsed())
        
        
        
if __name__ == '__main__':
    app = QApplication([])
    win = Window()
    win.show()
    app.exec_()
