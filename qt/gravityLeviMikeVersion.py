from PyQt5.QtCore import QElapsedTimer, QTimer, QSize, QPointF
from PyQt5.QtGui import QPalette
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QHBoxLayout

class Planetoid:

    def __init__(self, initPos, initVel, mass, movable=True):
        self.pos = initPos
        self.vel = initVel
        self.mass = mass
        self.movable = movable
        self.maxVel = 300.0

    def computePullToward(self, other):
        """Compute the pull this planetoid makes on the passed-in planetoid."""
        vec1 = self.pos - other.pos
        mag1 = magQPointF(vec1)
        gravityStrength = 200.0
        cutoff = 150.0
        powIndex = 1.8
        if mag1 < cutoff:
            m = gravityStrength*(self.mass + other.mass)/math.pow(cutoff, powIndex)
            forceMag = scale3_2(0.0, mag1, cutoff, -2*m, m)
        else:
            forceMag = 200*(self.mass + other.mass)/math.pow(mag1, powIndex)
        # scale magnitude down to zero
        return normalizeQPointF(vec1) * forceMag

    def draw(self, painter):
        painter.drawEllipse(QRectF(self.pos-QPointF(2, 2), self.pos + QPointF(10, 10)))

    def update(self, others, deltaT):
        if not self.movable:
            return
        f = QPointF(0, 0)
        for o in others:
            f = f + o.computePullToward(self)
        m = magQPointF(f)
        self.vel = self.vel + f
        if magQPointF(self.vel) > self.maxVel:
            self.vel = self.maxVel*normalizeQPointF(self.vel)
        self.pos = self.pos + deltaT*self.vel/4000


class RenderArea(QWidget):
    def __init__(self, parent=None):
        super(RenderArea, self).__init__(parent)
        self.setBackgroundRole(QPalette.Base)
        self.setAutoFillBackground(True)
        p1 = Planetoid(initPos = QPointF(275, 275),
                       initVel = QPointF( 30,   -120),
                       mass = 100,
                       movable = True)
        p2 = Planetoid(QPointF(400, 400), QPointF(0,   0), 200, False)
        p3 = Planetoid(QPointF(650, 550), QPointF(90,   -40), 100, True)
        p4 = Planetoid(QPointF(275, 600), QPointF(-90, 10), 100, True)
        self.ps = [p1, p2, p3, p4]
        self.t = 0

    def minimumSizeHint(self):
        return QSize(400, 400)

    def sizeHint(self):
        return QSize(800, 800)

    def timeUpdate(self, t):
        deltaT = t-self.t
        self.t = t

        l = len(self.ps)
        for i in range(l):
            others = [self.ps[j] for j in range(l) if j != i]
            self.ps[i].update(others, deltaT)
        #
        self.update()

    def paintEvent(self, event):
        pnt = QPainter(self)
        pnt.setRenderHint(QPainter.Antialiasing)
        # pnt.drawLine(QPointF(0, 0), QPointF(100, 200))
        pnt.setBrush(QBrush(Qt.black))
        for p in self.ps:
            p.draw(pnt)
        # pnt.drawEllipse(QRectF(QPointF(0, 0), QPointF(100, 100)))

class Window(QWidget):
    def __init__(self):
        super(Window, self).__init__()


        lo = QHBoxLayout()
        self.renderArea = RenderArea()
        lo.addWidget(self.renderArea)
        self.setLayout(lo)
        self.setWindowTitle("Drawing")

        '''
        self.elapsedTimer = QElapsedTimer()
        self.elapsedTimer.start()

        self.timer = QTimer(self)
        self.timer.setSingleShot(False)
        self.timer.timeout.connect(self.timeUpdate)
        self.timer.start(60)
        '''

    def keyPressEvent(self, evt):
        pass

    def timeUpdate(self):
        self.renderArea.timeUpdate(self.elapsedTimer.elapsed())


if __name__ == '__main__':
    app = QApplication([])
    win = Window()
    win.show()
    app.exec_()