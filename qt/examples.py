from PyQt5.QtWidgets import QWidget, QHBoxLayout, QApplication
from PyQt5.QtCore import (QPointF, QPoint, QSize, QTimer, Qt, QElapsedTimer,
                          QRectF)

from PyQt5.QtGui import (QPalette, QPen, QPainter, QPolygonF, QFont,
                         QPolygonF)
import math


def unitCircle(angle):
    return QPointF(math.cos(angle), math.sin(angle))


def computeEqTri(center, radius):
    # so descender, radius and 1/2 side length
    # let's say descender is 1. radius is 2. half side length is
    # sqrt(3). side length is 2*sqrt(3).
    #
    #
    # say radius is R. (radius:side length) is 2:sqrt(3)
    #
    # so side length is R*sqrt(3)
    # descender is radius/2
    #
    # descender is d. L = 2*sqrt(3) * d
    #
    # d = L / (2*sqrt(3))
    #
    # descender is 2*d
    #
    sideLength = radius*math.sqrt(3)
    d = radius/2.0
    p1 = center + QPointF(  sideLength/2.0,   d)
    p2 = center + QPointF(- sideLength/2.0,   d)
    p3 = center + QPointF(             0.0, -2*d)
    return QPolygonF([p1, p2, p3])


class RenderArea(QWidget):
    def __init__(self, parent=None):
        super(RenderArea, self).__init__(parent)
        self.setBackgroundRole(QPalette.Base)
        self.setAutoFillBackground(True)
        self.antialiased = True

        # self.rotation = 0.0
        self.t = 0.0
        self.ang = 0.0
        self.state = 1
        # self.deltaT = -1.0
        # self.radius = 100.0
        # self.center = QPointF(200, 200)

    def minimumSizeHint(self):
        return QSize(400, 400)

    def sizeHint(self):
        return QSize(750, 750)

    def paintEvent(self, event):
        self.ang += self.state*0.025
        painter = QPainter(self)
        pen = QPen()
        pen.setWidth(2)
        painter.setRenderHint(QPainter.Antialiasing)
        painter.setPen(pen)

        s = self.size()

        center = QPointF(s.width()/2, s.height()/2)
        shapeCenter = center + 100.0 * unitCircle(-2*self.ang)
        
        radius = 100.0

        ratio = 2 + math.sin(self.ang * 2.0)


        triAngleAng = self.ang
        pts = []
        n = 8
        
        for i in range(n):
            pts.append(shapeCenter + (unitCircle(triAngleAng) *
                                      (radius * ratio)))
            triAngleAng += (3 * math.pi * 2 / n)
        painter.drawPolygon(QPolygonF(pts))

    def timeUpdate(self, t):
        self.t = t
        self.update()

    def change(self, key):
        if key == Qt.Key_A:
            self.state = 0
        elif key == Qt.Key_Space:
            self.state = -1
        elif key == Qt.Key_Return:
            self.state = 1



class Window(QWidget):
    def __init__(self):
        super(Window, self).__init__()
        self.renderArea = RenderArea()
        lo = QHBoxLayout()
        lo.addWidget(self.renderArea)
        self.setLayout(lo)
        self.setWindowTitle("Drawing")
        self.elapsedTimer = QElapsedTimer()
        self.elapsedTimer.start()

        self.timer = QTimer(self)
        self.timer.setSingleShot(False)
        self.timer.timeout.connect(self.timeUpdate)
        self.timer.start(25)

    def keyPressEvent(self, evt):
        if evt.key() == Qt.Key_Q:
            QApplication.quit()
            
        self.renderArea.change(evt.key())

    def timeUpdate(self):
        self.renderArea.timeUpdate(self.elapsedTimer.elapsed())



if __name__ == '__main__':
    app = QApplication([])
    win = Window()
    win.show()
    app.exec_()
