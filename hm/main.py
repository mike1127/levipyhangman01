import re

def setLetter(letter, t):
    if t[0] == letter:
        return (t[0], True)
    return t

def checkInput():
    while True:
        c = input("Enter a character: ")
        if re.match(r"[a-z]$", c):
            return c
        print(c + ": is not valid. Enter a single character.")

def checkIfComplete(w):
    return all([t[1] for t in w])

def setLetterAll(c, wordTuple):
    output = []
    for t in wordTuple:
        if t[0] == c:
            output.append((t[0], True))
        else:
            output.append(t)
    return output


# ds = [c[0] for c in d if c[1]]

# w = [('b', True), ('a', False)]
def checkForWin(w):
    return all([t[1] for t in w])

def toRepresentation(t):
    return t[0] if t[1] else ' _ '

def repr(w):
    rep = [t[0].upper() if t[1] else ' _ ' for t in w]
    return "".join(rep)


def printCharactersEntered(cSet):
    print(sorted(list(cSet)))

    print("Entered:", end=" ")
    for c in cSet:
        print(c, end=" ")
    print()

def main():
    charactersEntered = set([])
    word ="antidisestablishmentarianism"
    wordTuple1 = [(i.lower(), False) for i in list(word)]
    while True:
        print(repr(wordTuple1))
        printCharactersEntered(charactersEntered)
        char = checkInput()
        charactersEntered.add(char)
        wordTuple1 = setLetterAll(char, wordTuple1)
        if checkForWin(wordTuple1):
            print(charactersEntered)
            print("You win!")
            break


main()